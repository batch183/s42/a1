// console.log("Hello Wolrd")
// [SECTIOn] DOM
// allows us to access or modify the properties of an element in a webpage
// it is a standard on how to get, change, add, or delete HTML elements

/*

	Syntax: document.querySelector("htmlElement");

	- The querySelector function takes a string input that is formatted like a CSS Selector when applying styles.

*/

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");
const color = document.querySelector("#color");

// [SECTION] Event Listeners
// Mous Click, Mouse Hover, Page Load, Key Press, etc

/*txtFirstName.addEventListener("keyup", () => {
	spanFullName.innerHTML = `${txtFirstName.value}`;
})

txtFirstName.addEventListener("keyup", (event) => {
	// element where event happened
	console.log(event.target);
	//value of the input
	console.log(event.target.value);
})*/


const fullName = () => {
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
}

const changeColor = () => {
	spanFullName.style.color = color.value;
}
txtFirstName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);
color.addEventListener("change",changeColor);

